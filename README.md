# ORImageGallery

[![CI Status](http://img.shields.io/travis/Evgeny Ivanov/ORImageGallery.svg?style=flat)](https://travis-ci.org/Evgeny Ivanov/ORImageGallery)
[![Version](https://img.shields.io/cocoapods/v/ORImageGallery.svg?style=flat)](http://cocoapods.org/pods/ORImageGallery)
[![License](https://img.shields.io/cocoapods/l/ORImageGallery.svg?style=flat)](http://cocoapods.org/pods/ORImageGallery)
[![Platform](https://img.shields.io/cocoapods/p/ORImageGallery.svg?style=flat)](http://cocoapods.org/pods/ORImageGallery)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ORImageGallery is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ORImageGallery"
```

## Author

Evgeny Ivanov, evgeny.ivanov@omega-r.com

## License

ORImageGallery is available under the MIT license. See the LICENSE file for more info.
